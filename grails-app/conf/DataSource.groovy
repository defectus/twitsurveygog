import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsAnnotationConfiguration
   dataSource {
       configClass = GrailsAnnotationConfiguration.class
   	   pooled = false
   	   driverClassName = "com.mysql.jdbc.Driver"
   	   username = "twitsurvey"
   	   password =  "janeka"
   	   dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
   }
   hibernate {
       cache.use_second_level_cache=true
       cache.use_query_cache=true
       cache.provider_class='com.opensymphony.oscache.hibernate.OSCacheProvider'
   }
   // environment specific settings
   environments {
   	development {
   		dataSource {
   			dbCreate = "update" // one of 'create', 'create-drop','update'
   			url = "jdbc:mysql://localhost:3306/twitsurvey"
   		}
   	}
   	test {
   		dataSource {
   			dbCreate = "update"
   			url = "jdbc:mysql://localhost:3306/twitsurvey"
   		}
   	}
   	production {
   		dataSource {
   			dbCreate = "update"
   			url = "jdbc:mysql://mysql-twitsurvey.jelastic.com/twitsurvey"
   		}
   	}
   }